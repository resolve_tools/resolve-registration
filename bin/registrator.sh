#!/usr/bin/env bash
set -euo pipefail

IN_FOLDER=$1
OUT_FOLDER=$2
DO_MINDAGAP=$3

MINDAGAP="/MindaGap/mindagap.py" # Path inside the container

CONTAINER="singularity/containers/toolbox_latest.sif" 
if [ ! -f $CONTAINER ]
then
	echo -e"Getting the singularity container"
	mkdir  -p "singularity/containers"
	singularity pull --arch amd64 library://michelebortol/resolve_tools/toolbox:latest
	mv toolbox.sif singularity/containers/
fi

# Get the target DAPI image from resolve
dst=$(find "$IN_FOLDER/" -name "*_Channel3_R8_.tiff" ! -name "._*")

# Get the source DAPI image from the dragonfly
src=$(find "$IN_FOLDER/" -name "*405nm*")

# Get the other images to register
other=$(find "$IN_FOLDER/" -regextype posix-extended -regex ".+[0-9]{3}nm.+" ! -name "*405nm*")

# Make output folder
mkdir -p $OUT_FOLDER

if [ "$DO_MINDAGAP" = "true" ]
then
	echo -e "FILLING IN GAPS:\n$dst"
	singularity exec $CONTAINER python3 $MINDAGAP $dst 3 
	gridfilled="${dst%.tiff}""gridfilled.tiff" 
	mv $gridfilled $OUT_FOLDER/
	echo -e "FILLED IMAGE:\n$OUT_FOLDER/""${gridfilled##*/}"
	dst="$OUT_FOLDER/""${gridfilled##*/}"
fi

# Register the images
echo -e "REGISTERING:\n$src\nTO:\n$dst\nTOGHETHER WITH:\n$other."
singularity exec $CONTAINER python3 bin/SIFT_registration.py $src $dst $OUT_FOLDER $other 



